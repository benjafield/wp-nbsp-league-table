# New Bradwell St Peter League Table

## Introduction

This Wordpress Plugin allows you to compile the Spartan South Midlands Football League Division 2 using a shortcode.

## Installation

I'm not hosting this on wordpress so you'll have to copy the files into your __/wp-content/plugins__ directory or, use the better method of cloning this repository into it.

You can do this by navigating to the __/wp-content/plugins__ directory in your CLI and running the following command...

```
git clone https://benjafield@bitbucket.org/benjafield/wp-nbsp-league-table.git
```

This will create a folder called __wp-nbsp-league-table__ with the plugin files inside.

## Usage

The league table can be inserted into a post using the following shortcode:

```
[nbsp-league-table]
```

### Shortcode Attributes

The following attributes are available on the shortcode...

| Attribute    | Description                                               | Default        | Example                                    |
| ------------ | --------------------------------------------------------- | -------------- | ------------------------------------------ |
| class        | Adds a class to the table's containing element.           | *Empty String* | `[nbsp-league-table class="customClass"]`  |
| lrcode       | If you wish to use a different lrcode from the FA website | 493108679      | `[nbsp-league-table lrcode="0123456789"]`  |
| id           | The ID of the containing element.                         | * No Default * | `[nbsp-league-table id="myId"]`            |

That's it! Happy coding :)
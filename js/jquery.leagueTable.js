(function($) {

	$.fn.leagueTable = function(options) {
		var data = [], el = $(this);
		el.find('div').hide();
		var iteration = 0;

		var compileTable = function() {
			iteration++;

			el.find('table tbody tr').each(function() {
				var obj = [], $td = $(this).find('td');
				for(var i = 0; i < 8; i++) {
					obj[i] = $.trim($td.eq(i).text());
				}
				data.push(obj);
			});
			console.log(data);

			var className = 'league-table' || options.className,
				$table = $('<table>').addClass(className),
				$tableHeader = $('<thead>'), $tableHeadRow = $('<tr>'), $tableBody = $('<tbody>'),
				tableHeadings =  ['#', 'Team', 'P', 'W', 'D', 'L', 'GD', 'Pts'] || options.headings;

			for(var i = 0; i < tableHeadings.length; i++) {
				$tableHeadRow.append('<th>'+tableHeadings[i]+'</th>');
			}

			$tableHeader.append($tableHeadRow);

			for(var i = 1; i < (data.length - 2); i++){
				var active = (data[i][1] == 'New Bradwell St Peter') ? ' class="active-club"' : '';
				var $tableRow = $('<tr' + active + '>');

				for(var x = 0; x < data[i].length; x++) {
					$tableRow.append('<td>'+data[i][x]+'</td>');
				}
				$tableBody.append($tableRow);
			}
			el.html('');
			$table.append($tableHeader).append($tableBody);
			el.append($table);
		};

		var checkForData = setInterval(function() {
			if(el.find('table').length) {
				clearInterval(checkForData);
				compileTable();
			}
		}, 100);
	};
	$(function() {
		$('.leagueTable').leagueTable();
	});
})(jQuery);
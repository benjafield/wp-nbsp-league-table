<?php
/*
Plugin Name: New Bradwell St Peter League Table
Plugin URI: https://bitbucket.org/benjafield/wp-nbsp-league-table
Description: Simple plugin go build the Spartan league table.
Version: 1.0.0
Author: Charlie Benjafield
Author URI: http://cbenjafield.com
*/

/**
 * Enqueue the jQuery Script
 */
function nbspLoadScripts()
{
	wp_enqueue_script( 'nbsp-league-table', plugin_dir_url( __FILE__ ) . 'js/jquery.leagueTable.min.js', array('jquery'), '1.0.0', true );
}
add_action('wp_enqueue_scripts', 'nbspLoadScripts');

/**
 * Insert the code snippet from the FA Full-Time website.
 */
function nbspLeageTable($atts = array())
{
	$a = shortcode_atts(array(
		'class' => '',
		'id' => '',
		'lrcode' => '493108679'
	), $atts);

	return '<div class="leagueTable '.$a['class'].'"'.((!empty($a['id'])) ? ' id="'.$a['id'].'"' : '').'>
				<div id="lrep493108679">Loading...</div>
				<script language="javascript" type="text/javascript">
					var lrcode = "'.$a['lrcode'].'";
				</script>
				<script language="Javascript" type="text/javascript" src="http://full-time.thefa.com/client/api/cs1.js"></script>
			</div>';
}
add_shortcode('nbsp-league-table', 'nbspLeageTable');